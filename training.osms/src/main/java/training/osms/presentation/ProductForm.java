package training.osms.presentation;

import java.util.List;

import javax.faces.context.FacesContext;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.jsf.FacesContextUtils;

import training.osms.business.Category;
import training.osms.business.CategoryController;
import training.osms.business.CategorySearchOptions;
import training.osms.business.Product;

public class ProductForm {
	
	private Product product;
	private Category category;
	private List<Category> categories;
	private CategoryController controller;
	
	public ProductForm() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		WebApplicationContext applicationContext = FacesContextUtils.getWebApplicationContext(facesContext);
		
		controller = applicationContext.getBean(CategoryController.class);
		categories = controller.search(new CategorySearchOptions());
		product = new Product();
		category = new Category();
	}
	
	public Product getProduct() {
		return product;
	}
	
	public void setProduct(Product product) {
		this.product = product;
	}
	
	public Category getCategory() {
		return category;
	}
	
	public void setCategory(Category category) {
		this.category = category;
	}
	
	public List<Category> getCategories() {
		return controller.search(new CategorySearchOptions());
	}
	
	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}
	
	public Integer getCategoryId(){
		
		Category category = product.getCategory();
		if(category == null){
			return null;
		}else{
			return category.getId();
		}
		
	}
	
	public void setCategoryId(Integer categoryId){
		if(categoryId == null){
			product.setCategory(null);
		}else{
			for(Category category: categories){
				if(categoryId.equals(category.getId())){
					product.setCategory(category);
					break;
				}
			}
		}
	}
	
}
