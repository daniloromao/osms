package training.osms.presentation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import training.osms.business.BusinessException;
import training.osms.business.ProductController;

@Component
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class NewProduct {

	private ProductForm form;
	@Autowired
	private ProductController controller;
	
	public NewProduct() {
		form = new ProductForm();
	}
	
	public ProductForm getForm() {
		return form;
	}
	public void setForm(ProductForm form) {
		this.form = form;
	}
	
	public void insertProduct(){
		try{
			controller.insertProduct(form.getProduct());
			new MyFacesMessage("Product successfully registered");
		}catch(BusinessException e){
			new MyFacesMessage(e.getMessage());
		}
	}
}
