package training.osms.presentation;

import java.util.Date;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import training.osms.business.Product;
import training.osms.business.ProductController;
import training.osms.business.Request;
import training.osms.business.RequestController;

@Component
@Scope(WebApplicationContext.SCOPE_SESSION)
public class ShowProduct {
	
	@Autowired
	private ProductController pController;
	@Autowired
	private RequestController rController;
	
	private Product product;
	private Integer productId;
	private Request request;
	
	@PostConstruct
	private void initialize(){
		reset();
	}
	
	public void reset(){
		request = new Request();
	}
	
	public Integer getProductId(){
		return productId;
	}
	
	public void setProductId(Integer productId) {
		this.productId = productId;
		product = pController.searchProduct(productId);
	}
	
	public Product getProduct() {
		return product;
	}
	
	public void setProduct(Product product) {
		this.product = product;
	}
	
	public String showProduct(Product product){
		this.product = product;
		return "showProduct";
	}
	
	public Request getRequest() {
		return request;
	}
	
	public void setRequest(Request request) {
		this.request = request;
	}
	
	public String addToRequest(){
		request.getProducts().add(product);
		return "showRequest";
	}
	
	public void removeFromRequest(Product product){
		request.getProducts().remove(product);
	}
	
	public Double getTotal(){
		return request.getTotal();
	}
	
	public String checkout(){
		request.setDate(new Date());
		rController.insertRequest(request);
		reset();
		return "requestFinished";
	}
}