package training.osms.presentation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import training.osms.business.BusinessException;
import training.osms.business.Product;
import training.osms.business.ProductController;
import training.osms.business.ProductSearchOptions;

@Component
@Scope(WebApplicationContext.SCOPE_SESSION)
public class SearchProduct {
	
	public static final int PRODUCT_PER_PAGE = 5;
	
	@Autowired
	private ProductController controller;
	
	private ProductForm form;
	private List<Product> products;
	private ProductSearchOptions options;
	private boolean deleted;
	private List<Integer> pages;
	private int page;
	
	
	public SearchProduct() {
		form = new ProductForm();
		reset();
	}
	
	public ProductForm getForm() {
		return form;
	}
	
	public void setForm(ProductForm form) {
		this.form = form;
	}
	
	public List<Product> getProducts() {
		return products;
	}
	
	public void setProducts(List<Product> products) {
		this.products = products;
	}
	
	public ProductSearchOptions getOptions() {
		return options;
	}
	
	public void setOptions(ProductSearchOptions options) {
		this.options = options;
	}
	
	public List<Integer> getPages() {
		return pages;
	}
	
	public void setPages(List<Integer> pages) {
		this.pages = pages;
	}

	public boolean isShowTable(){
		if(products == null){
			return false;
		}else{
			return !products.isEmpty();
		}
	}
	
	public boolean isDeleted() {
		return deleted;
	}

	public void searchProducts(){
		int productCount;
		int pagesCount;
		
		productCount = controller.searchProductCount(options);
		
		pagesCount = productCount / PRODUCT_PER_PAGE;
		
		if(productCount%PRODUCT_PER_PAGE != 0){
			pagesCount++;
		}
		pages = new ArrayList<Integer>();
		for(int i = 1;i <= pagesCount; i++){
			pages.add(i);
		}
		
		goToPage(1);
	}
		
	public void updateProduct(){
		try{
			controller.updateProduct(form.getProduct());
			reset();
			new MyFacesMessage("Product update sucessfulluy");
		}catch(BusinessException e){
			new MyFacesMessage(e.getMessage());
		}
		
	}
	
	public String showUpdateProduct(Product product){
		Product copy = product.clone();
		form.setProduct(copy);
		return "updateProduct";
	}
	
	public void reset(){
		products = new ArrayList<Product>();
		options = new ProductSearchOptions();
		pages = new ArrayList<Integer>();
		form.setProduct(new Product());
	}
	
	public String showDeleteProduct(Product product){
		Product copy = product.clone();
		form.setProduct(copy);
		deleted = false;
		return "deleteProduct";
	}
	
	public void deleteProduct(){
		try {
			controller.deleteProduct(form.getProduct());
			deleted = true;
			reset();
			new MyFacesMessage("Product deleted successfully");
		} catch (Exception e) {
			new MyFacesMessage("This product can not be deleted by being tied to a sale");
		}
	}
	
	public void goToPage(int page){
		int firstResult = (page - 1) * PRODUCT_PER_PAGE;
		this.page = page;
		options.setMaxResults(PRODUCT_PER_PAGE);
		options.setFirstResult(firstResult);
		products = controller.searchProduct(options);
		
		if(products.size() == 0){
			new MyFacesMessage("No product found");
		}
	}
	
	public boolean isActualPage(int page){
		return this.page == page;
	}
}
