package training.osms.presentation;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import training.osms.business.Category;
import training.osms.business.CategoryController;
import training.osms.business.Product;
import training.osms.business.ProductController;
import training.osms.business.ProductSearchOptions;

@Component
@Scope(WebApplicationContext.SCOPE_SESSION)
public class ShowCategory {
	
	public static final int PRODUCT_PER_PAGE = 4;
	
	@Autowired
	private CategoryController controller;
	@Autowired
	private ProductController pController;
	
	private List<Category> categories;
	private Category category;
	private List<Product> products;
	private List<Category> breadCrumb;
	private List<Integer> categoriesId;
	private List<Integer> pages;
	private ProductSearchOptions options;
	private int page;
	private DecimalFormat df;
	

	@PostConstruct
	public void initialize(){
		breadCrumb = new ArrayList<Category>();
		categories = controller.searchCategoryFathers();
		options = new ProductSearchOptions();
		pages = new ArrayList<Integer>();
		df = new DecimalFormat("##,###,###,###,##0.00");
	}
	
	public List<Category> getCategories() {
		return categories;
	}
	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}
	public Category getCategory() {
		return category;
	}

	public List<Product> getProducts() {
		return products;
	}
	public Integer getCategoryId(){
		return 0;
	}
	public List<Integer> getPages() {
		return pages;
	}
	public void setPages(List<Integer> pages) {
		this.pages = pages;
	}

	public List<Category> getBreadCrumb() {
		return breadCrumb;
	}
	
	public void getCategoriesId(Category category){
		if(category != null){	
			categoriesId.add(category.getId());
			for(Category son : category.getSons()){
				getCategoriesId(son);
			}
		}
	}
	
	public void setCategoryId(Integer categoryId) {
		category = controller.search(categoryId);

		if(category != null){
			if(!category.getSons().isEmpty())
				categories = category.getSons();
		
			categoriesId = new ArrayList<>();			
			getCategoriesId(category);
			
			options.setCategoriesId(categoriesId);
			
			setShowBreadCrumb(category);
			
			searchProducts();
		}
	}
	
	public void searchProducts(){
		int productCount = pController.searchProductCount(options);
		
		int pagesCount = productCount / PRODUCT_PER_PAGE;
		
		if(productCount % PRODUCT_PER_PAGE != 0){
			pagesCount++;
		}
		
		pages = new ArrayList<Integer>();
		
		for(int i = 1;i <= pagesCount; i++){
			pages.add(i);
		}
		
		goToPage(1);
	}
	
	public void goToPage(Integer page) {
		this.page = page;
		int firstResult = (page - 1) * PRODUCT_PER_PAGE;
		
		options.setMaxResults(PRODUCT_PER_PAGE);
		options.setFirstResult(firstResult);
		products = pController.searchProduct(options);
		if(products.size() == 0){
			new MyFacesMessage("This category doesn't have products!");
		}
		
	}
	
	public void setShowBreadCrumb(Category category){
		breadCrumb = new ArrayList<Category>();
		
		breadCrumb.add(category);
		
		while(category.getFather() != null){
			breadCrumb.add(category.getFather());
			category = category.getFather();
		}
		Collections.reverse(breadCrumb);
	}
	
	public boolean lastCategory(Category category){
		return breadCrumb.indexOf(category) == (breadCrumb.size() - 1);
	}
	public String clearBreadCrumb(){
		breadCrumb = new ArrayList<Category>();
		categories = controller.searchCategoryFathers();
		return "searchProduct";
	}
	
	public String getProductName(Product product){
		if(product.getName().length() <= 25)
			return product.getName();
		else
			return product.getName().substring(0, 25)+"...";
	}
	
	public void nextPage(){
		if(page < pages.size()){
			goToPage(page + 1);
		}
	}
	
	public void previousPage(){
		int firstPage = 1;
		if(page > firstPage){
			goToPage(page - 1);
		}
	}
	
	public boolean isFirstPage(){
		int firstPage = 1;
		return page <= firstPage;
	}
	
	public boolean isLastPage(){
		return page >= pages.size();
	}
	
	public boolean isActualPage(int page){
		return this.page == page;
	}
	public String getProductPrice(Product product){
		if(product == null)
			return null;
			
		return df.format(product.getPrice());
	}
	
}
