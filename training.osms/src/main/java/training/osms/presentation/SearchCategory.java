package training.osms.presentation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import training.osms.business.BusinessException;
import training.osms.business.Category;
import training.osms.business.CategoryController;
import training.osms.business.CategorySearchOptions;

@Component
@Scope(WebApplicationContext.SCOPE_SESSION)
public class SearchCategory {
	
	public static int CATEGORY_PER_PAGE = 5;
	
	@Autowired
	private CategoryController controller;
	
	private CategoryForm form;
	private List<Category> categories; 
	private CategorySearchOptions options;
	private boolean deleted;
	private List<Integer> pages;
	private int page;
	
	public SearchCategory(){
		form = new CategoryForm();
		options = new CategorySearchOptions();
		pages = new ArrayList<Integer>();
	}
	
	
	public CategoryForm getForm() {
		return form;
	}
	public void setForm(CategoryForm form) {
		this.form = form;
	}
	public CategorySearchOptions getOptions() {
		return options;
	}
	public void setOptions(CategorySearchOptions options) {
		this.options = options;
	}
	public List<Category> getCategories() {
		return categories;
	}
	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}
	public boolean isDeleted() {
		return deleted;
	}
	public List<Integer> getPages() {
		return pages;
	}
	public void setPages(List<Integer> pages) {
		this.pages = pages;
	}
	
	public boolean isShowTable(){
		if(categories == null){
			return false;
		}else{
			return !categories.isEmpty();
		}
	}
	
	public void searchCategory(){
		int quantResults;
		int pagesCount;
		quantResults = controller.searchCount(options);
		
		pagesCount = quantResults / CATEGORY_PER_PAGE;
		if(quantResults%CATEGORY_PER_PAGE != 0){
			pagesCount++;
		}
		pages = new ArrayList<Integer>();
		for(int i = 1; i <= pagesCount;i++){
			pages.add(i);
		}
		
		goToPage(1);
	}
	
	public String showUpdateCategory(Category category){
		form.setCategories(controller.search(new CategorySearchOptions()));
		Category copy = category.clone();
		form.setCategory(copy);

		return "updateCategory";
	}
	
	public void updateCategory(){
		try{
			controller.update(form.getCategory());
			new MyFacesMessage(form.getCategory().getName()+" Successful update!");
			reset();
		}catch(BusinessException e){
			new MyFacesMessage(e.getMessage());
		}
	}
	
	public void reset(){
		categories = new ArrayList<Category>();
		pages = new ArrayList<Integer>();
		page = 0;
		form.setCategory(new Category());
	}

	
	public String showDeleteCategory(Category category){
		Category copy = category.clone();
		form.setCategory(copy);
		deleted = false;
		return "deleteCategory";
	}
	
	public void deleteCategory(){
		controller.delete(form.getCategory());
		new MyFacesMessage("Category deleted successfully");
		deleted = true;
		reset();
	}
	
	public void goToPage(int page){
		int firstResult = (page - 1) * CATEGORY_PER_PAGE;
		options.setMaxResults(CATEGORY_PER_PAGE);
		options.setFirstResult(firstResult);
		this.page = page;
		categories = controller.search(options);
		
		if(categories.size() == 0){
			new MyFacesMessage("No category found");
		}
	}
	public boolean isActualPage(int page){
		return this.page == page;
	}
}
