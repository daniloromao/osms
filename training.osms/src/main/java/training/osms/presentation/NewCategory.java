package training.osms.presentation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import training.osms.business.BusinessException;
import training.osms.business.CategoryController;

@Component
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class NewCategory {

	private CategoryForm form;
	@Autowired
	private CategoryController controller;

	public NewCategory() {
		form = new CategoryForm();
	}
	
	public CategoryForm getForm() {
		return form;
	}
	public void setForm(CategoryForm form) {
		this.form = form;
	}

	public void insertCategory(){
		try{
			controller.insert(form.getCategory());
			form = new CategoryForm();
			new MyFacesMessage("Successfully registered category");
		}catch(BusinessException e){
			new MyFacesMessage(e.getMessage());
		}
	}
}
