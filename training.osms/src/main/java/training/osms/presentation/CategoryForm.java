package training.osms.presentation;

import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.jsf.FacesContextUtils;

import training.osms.business.Category;
import training.osms.business.CategoryController;
import training.osms.business.CategorySearchOptions;
import training.osms.business.Product;

public class CategoryForm {
	
	private Category category;
	private List<Category> categories;
	private Category father;
	private List<Product> products;
	
	public CategoryForm(){
		FacesContext facesContext = FacesContext.getCurrentInstance();
		WebApplicationContext applicationContext = FacesContextUtils.getWebApplicationContext(facesContext);
		
		CategoryController controller = applicationContext.getBean(CategoryController.class);
		categories = controller.search(new CategorySearchOptions());

		products = new ArrayList<Product>();
		father = new Category();
		category = new Category();
	}
	
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	public List<Category> getCategories() {
		return categories;
	}
	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}
	public Category getFather() {
		return father;
	}
	public void setFather(Category father) {
		this.father = father;
	}
	public List<Product> getProducts() {
		return products;
	}
	public void setProducts(List<Product> products) {
		this.products = products;
	}
	
	public Integer getFatherId(){
		Category father = category.getFather();
		if(father == null){
			return null;
		}else{
			return father.getId();
		}
	}
	public void setFatherId(Integer fatherId){
		if(fatherId == 0){
			category.setFather(null);
		}else{
			for(Category father : categories){
				if(fatherId.equals(father.getId())){
					category.setFather(father);
					break;
				}
			}
		}
	}

}
