package training.osms.presentation;

import java.math.RoundingMode;
import java.text.DecimalFormat;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@FacesConverter(value="DecimalConverter")
public class DecimalConverter implements Converter{

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		throw new UnsupportedOperationException();
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		Double numberValue = (Double)value;
		DecimalFormat df = new DecimalFormat("###,###,###,###,##0.00");
		df.setRoundingMode(RoundingMode.UP);
		return df.format(numberValue);
		
	}

}
