package training.osms.presentation;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import training.osms.business.Product;
import training.osms.business.Request;
import training.osms.business.RequestController;
import training.osms.business.RequestSearchOptions;

@Component
public class SearchRequest {
	
	public static final int REQUEST_PER_PAGE = 5;
	
	@Autowired
	private RequestController controller;
	private RequestSearchOptions options;
	private List<Request> requests;
	private Request request;
	private int page;
	private List<Integer> pages;
	
	@PostConstruct
	private void initialize(){
		requests = new ArrayList<Request>();
		options = new RequestSearchOptions();
		pages = new ArrayList<Integer>();
	}
	
	public RequestSearchOptions getOptions() {
		return options;
	}
	public void setOptions(RequestSearchOptions options) {
		this.options = options;
	}
	public List<Request> getRequests() {
		return requests;
	}
	public void setRequests(List<Request> requests) {
		this.requests = requests;
	}

	public Request getRequest() {
		return request;
	}
	public void setRequest(Request request) {
		this.request = request;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public List<Integer> getPages() {
		return pages;
	}
	public void setPages(List<Integer> pages) {
		this.pages = pages;
	}
	public List<Product> getProducts(){
		if(request == null){
			return null;
		}
		Request newRequest = controller.searchRequest(request.getId());
		return newRequest.getProducts();
	}
	public void searchRequest(){
		int requestCount;
		int pagesCount;
		
		requestCount = controller.searchRequestCount(options);
		
		pagesCount = requestCount / REQUEST_PER_PAGE;
		
		if(requestCount%REQUEST_PER_PAGE != 0){
			pagesCount++;
		}
		pages = new ArrayList<Integer>();
		for(int i = 1;i <= pagesCount; i++){
			pages.add(i);
		}
		
		goToPage(1);
	}
	public void goToPage(int page){
		int firstResult = (page - 1) * REQUEST_PER_PAGE;
		this.page = page;
		options.setMaxResults(REQUEST_PER_PAGE);
		options.setFirstResult(firstResult);
		requests = controller.searchRequest(options);
		
		if(requests.size() == 0){
			new MyFacesMessage("No request found");
		}
	}
	
	public boolean isActualPage(int page){
		return this.page == page;
	}
	
}
