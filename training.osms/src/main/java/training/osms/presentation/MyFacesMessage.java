package training.osms.presentation;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

public class MyFacesMessage {
	
	public MyFacesMessage(String summary) {
		
		FacesMessage message = new FacesMessage();
		message.setSeverity(FacesMessage.SEVERITY_INFO);
		message.setSummary(summary);
		
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, message);
	}

}
