package training.osms.business;

import training.framework.business.AbstractEntitySearchOptions;

public class CategorySearchOptions extends AbstractEntitySearchOptions{
	
	private String name;
	private Integer maxResults;
	private Integer firstResult;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getMaxResults() {
		return maxResults;
	}
	public void setMaxResults(Integer maxResults) {
		this.maxResults = maxResults;
	}
	public Integer getFirstResult() {
		return firstResult;
	}
	public void setFirstResult(Integer firstResult) {
		this.firstResult = firstResult;
	}

}
