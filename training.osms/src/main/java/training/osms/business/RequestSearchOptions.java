package training.osms.business;

import java.util.Date;

public class RequestSearchOptions {
	
	private Date initDate;
	private Date endDate;
	private Integer MaxResults;
	private Integer firstResult;
	public Date getInitDate() {
		return initDate;
	}
	public void setInitDate(Date initDate) {
		this.initDate = initDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Integer getMaxResults() {
		return MaxResults;
	}
	public void setMaxResults(Integer maxResults) {
		MaxResults = maxResults;
	}
	public Integer getFirstResult() {
		return firstResult;
	}
	public void setFirstResult(Integer firstResult) {
		this.firstResult = firstResult;
	}
}
