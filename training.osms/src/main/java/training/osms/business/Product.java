package training.osms.business;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Entity
@Table(name="PRO_PRODUCT")
public class Product implements Cloneable{
	
	private Integer id;
	private String name;
	private String description;
	private Category category;
	private String image;
	private Double price;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="PRO_ID")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Size(min=1, max=100, message="Please specify name between {min} and {max} characters long")
	@Column(name="PRO_NM")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Size(min=1, max=1000, message="Please specify description between {min} and {max} characters long")
	@Column(name="PRO_DS")
	@Lob
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@NotNull
	@Column(name="PRO_PR", scale=2)
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CAT_ID")
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	@Column(name="PRO_IM")
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	@Override
	public String toString() {
		return "Product [name=" + name + ", description=" + description + ", category="+category.getName()+"]";
	}
	
	@Override
	public Product clone(){
		
		try {
			return (Product)super.clone();
		} catch (CloneNotSupportedException e) {
			throw new BusinessException("Interface Cloneable was not implemented");
		}
	}

}
