package training.osms.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import training.framework.persistence.IProductDao;

@Component
public class ProductController {

	@Autowired
	private IProductDao dao;

	public IProductDao getDao() {
		return dao;
	}
	
	public void setDao(IProductDao dao) {
		this.dao = dao;
	}

	@Transactional
	public void insertProduct(Product product){
		Product productDatabase = dao.search(product.getName());
		
		if(productDatabase == null){
			dao.insert(product);
		}else{
			throw new BusinessException("This product alredy exists");
		}
	}

	public List<Product> searchProduct(ProductSearchOptions options) {
		return dao.search(options);
		
	}

	@Transactional
	public void updateProduct(Product product) {
		Product databaseProduct = dao.search(product.getName());
		if(databaseProduct == null){
			dao.update(product);
		}else{
			if(databaseProduct.getId().equals(product.getId())){
				dao.update(product);
			}else{
				throw new BusinessException("This product name alredy exists");
			}
		}
	}

	@Transactional
	public void deleteProduct(Product product) {
		dao.delete(product);
		
	}

	public int searchProductCount(ProductSearchOptions options) {
		return dao.searchCount(options);
	}

	public Product searchProduct(Integer id) {
		return dao.find(id);
	}

}
