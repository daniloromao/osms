package training.osms.business;

import java.util.List;

import training.framework.business.AbstractEntitySearchOptions;

public class ProductSearchOptions extends AbstractEntitySearchOptions{
	
	private String name;
	private String description;
	private Integer category;
	private Double priceMax;
	private Double priceMin;
	private List<Integer> categoriesId;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Double getPriceMax() {
		return priceMax;
	}
	public void setPriceMax(Double priceMax) {
		this.priceMax = priceMax;
	}
	public Double getPriceMin() {
		return priceMin;
	}
	public void setPriceMin(Double priceMin) {
		this.priceMin = priceMin;
	}
	public Integer getCategory() {
		return category;
	}
	public void setCategory(Integer category) {
		this.category = category;
	}
	public List<Integer> getCategoriesId() {
		return categoriesId;
	}
	public void setCategoriesId(List<Integer> categoriesId) {
		this.categoriesId = categoriesId;
	}
	
}
