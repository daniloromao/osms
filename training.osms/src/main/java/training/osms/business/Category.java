package training.osms.business;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import training.framework.business.IEntity;

@Entity
@Table(name="CAT_CATEGORY")
public class Category implements Cloneable, IEntity{
	
	private Integer id;
	private String name;
	private List<Category> sons;
	private List<Product> products;
	private Category father;
	
	public Category() {
		sons = new ArrayList<>();
		products = new ArrayList<>();
	}
	
	public Category(Integer id) {
		this.id = id;
		sons = new ArrayList<>();
		products = new ArrayList<>();
	}
	
	@Override
	@Id
	@Column(name="CAT_ID")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Override
	@Size(min=1, max=100, message="Please specify the category between 1 and 100 characters long")
	@Column(name="CAT_NM")
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@OneToMany(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_FATHER")
	public List<Category> getSons() {
		return sons;
	}
	
	public void setSons(List<Category> sons) {
		this.sons = sons;
	}
	
	@OneToMany(mappedBy="category")
	public List<Product> getProducts() {
		return products;
	}
	
	public void setProducts(List<Product> products) {
		this.products = products;
	}
	
	@ManyToOne
	@JoinColumn(name="ID_FATHER")
	public Category getFather() {
		return father;
	}
	
	public void setFather(Category father) {
		this.father = father;
	}
	
	@Override
	public String toString() {
		if(father == null){
			father = new Category();
		}
		StringBuilder output = new StringBuilder();
		output.append("[Name: "+ name +", Father: "+ father.getName()+", ");
		for(Category category : sons){
			output.append(" Son: ");
			output.append(category.getName());
		}
		for(Product prod : products){
			output.append(" Products: ");
			output.append(prod.getName());
		}
		output.append("]");
		
		return output.toString();
	}
	
	@Override
	public Category clone(){
		try {
			return (Category)super.clone();
		} catch (CloneNotSupportedException e) {
			throw new BusinessException("Not implemented cloneable interface!");
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

}
