package training.osms.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;


@Entity
@Table(name="REQ_REQUEST")
public class Request {
	
	private Integer id;
	private List<Product> products;
	private Double total;
	private Date date;
	
	public Request(){
		products = new ArrayList<Product>();
	}
	
	@Id
	@Column(name="REQ_ID")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	@ManyToMany(cascade=CascadeType.DETACH)
	@JoinTable(
			name="RQP_REQUEST_PRODUCT",
			joinColumns=@JoinColumn(name="REQUEST_ID"),
			inverseJoinColumns=@JoinColumn(name="PRODUCT_ID")
	)
	public List<Product> getProducts() {
		return products;
	}
	
	public void setProducts(List<Product> products) {
		this.products = products;
	}
	
	@Column(name="REQ_TO")
	public Double getTotal() {
		total = 0.0;
		for(Product product : products){
			total = total + product.getPrice();
		}
		return total;
	}
	
	public void setTotal(Double total) {
		this.total = total;
	}
	
	@Column(name="REQ_DT")
	public Date getDate() {
		return date;
	}
	
	public void setDate(Date date) {
		this.date = date;
	}
}
