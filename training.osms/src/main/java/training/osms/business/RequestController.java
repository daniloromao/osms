package training.osms.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import training.framework.persistence.IRequestDao;

@Component
public class RequestController {
	
	@Autowired
	private IRequestDao dao;
	
	@Transactional
	public void insertRequest(Request request){
		dao.insert(request);
	}
	
	public Request searchRequest(Integer id){
		return dao.find(id);
	}
	
	public List<Request> searchRequest(RequestSearchOptions options){
		return dao.select(options);
	}
	
	public Integer searchRequestCount(RequestSearchOptions options){
		return dao.selectCount(options);
	}

}
