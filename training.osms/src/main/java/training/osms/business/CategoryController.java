package training.osms.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import training.framework.business.AbstractEntityController;
import training.framework.persistence.ICategoryDao;

@Component
public class CategoryController extends AbstractEntityController<Category, CategorySearchOptions>{

	@Autowired
	private ICategoryDao dao;
	
	@Override
	protected ICategoryDao getDao() {
		return dao;
	}
	
	@Override
	protected void insertVerification(Category category) {
		Category categoryDatabase = dao.search(category.getName());
		if(categoryDatabase != null){
			throw new BusinessException("This category name alredy exists");
		}		
	}
	
	@Override
	protected void upadateVerification(Category category) {
		Category databaseCategory = dao.search(category.getName());
		
		if(category.getFather() != null){
			if(category.getId() == category.getFather().getId())
				throw new BusinessException("A category can not be her own father.!");
		}
		
		if(databaseCategory != null){
			if(!databaseCategory.getId().equals(category.getId())){
				throw new BusinessException("This category name alredy exists!");
			}
		}
		
		Category father = category.getFather();

		if(father == null){
			father = new Category();
		}
		
		while(father.getFather() != null){
			if(father.getFather().getId() == category.getId()){
				throw new BusinessException("Failed to update, the "+category.getName()
						+ " category is parent of the category " + category.getFather().getName());
			}
			father = father.getFather();
		}			
	}

	public List<Category> searchCategoryFathers() {
		return dao.searchCategoryFathers();
	}
	
}
