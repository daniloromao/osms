package training.osms.persistence;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import training.framework.persistence.ICategoryDao;
import training.framework.persistence.IProductDao;
import training.osms.business.Product;
import training.osms.business.ProductSearchOptions;
import training.osms.business.Request;

public class ProductDaoJDBC implements IProductDao {

    private static final String TABLE_NAME = "PRO_PRODUCT";
    private static final String KEY_COLUMN = "PRO_ID";
    private static final String COLUMN_NAME = "PRO_NM";
    private static final String COLUMN_DESCRIPTION = "PRO_DS";
    private static final String COLUMN_PRICE = "PRO_PR";
    private static final String COLUMN_CATEGORY = "CAT_ID";
    private static final String COLUMN_IMAGE = "PRO_IM";

    private JdbcTemplate manager;
    private SimpleJdbcInsert insertProduct;

    @Autowired
    private ICategoryDao categoryDao;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.manager = new JdbcTemplate(dataSource);
        this.insertProduct =
                new SimpleJdbcInsert(dataSource).withTableName(TABLE_NAME)
                        .usingColumns(COLUMN_NAME, COLUMN_DESCRIPTION, COLUMN_PRICE, COLUMN_CATEGORY, COLUMN_IMAGE)
                        .usingGeneratedKeyColumns(KEY_COLUMN);
    }

    @Override
    public void insert(Product product) {
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put(COLUMN_NAME, product.getName());
        parameters.put(COLUMN_DESCRIPTION, product.getDescription());
        parameters.put(COLUMN_PRICE, product.getPrice());
        parameters.put(COLUMN_CATEGORY, product.getCategory() == null ? "" : product.getCategory().getId());
        parameters.put(COLUMN_IMAGE, product.getImage());
        Number newId = insertProduct.executeAndReturnKey(parameters);
        product.setId(newId.intValue());
    }

    @Override
    public void update(Product product) {
        String sqlUpdate =
                "UPDATE " + TABLE_NAME + " SET " + COLUMN_NAME + " = ?, " + COLUMN_DESCRIPTION + " = ?, "
                        + COLUMN_PRICE + " = ?, " + COLUMN_CATEGORY + " = ?," + COLUMN_IMAGE + " = ? WHERE "
                        + KEY_COLUMN + " = ?";
        manager.update(sqlUpdate, product.getName(), product.getDescription(), product.getPrice(),
                product.getCategory() == null ? "" : product.getCategory().getId(), product.getImage(), product.getId());
    }

    @Override
    public void delete(Product product) {
        String sqlDelete = "DELETE FROM " + TABLE_NAME + " WHERE " + KEY_COLUMN + " = ?";
        manager.update(sqlDelete, product.getId());
    }

    @Override
    public List<Product> search(ProductSearchOptions options) {
        StringBuilder jpql = new StringBuilder("SELECT * FROM ");
        jpql.append(TABLE_NAME);
        jpql.append(" e WHERE 1=1 ");

        appendPredicate(jpql, options);
        List<String> query = new ArrayList<String>();
        setParameters(query, options);

        if (options.getMaxResults() != null) {
            jpql.append(" LIMIT ? ");
            query.add(options.getMaxResults().toString());
        }

        if (options.getFirstResult() != null) {
            jpql.append(" OFFSET ? ");
            query.add(options.getFirstResult().toString());
        }

        List<Product> products = manager.query(jpql.toString(), query.toArray(), new RowMapper<Product>() {

            @Override
            public Product mapRow(ResultSet rs, int rowNum) throws SQLException {
                Product product = new Product();
                product.setId(rs.getInt(KEY_COLUMN));
                product.setName(rs.getString(COLUMN_NAME));
                product.setDescription(rs.getString(COLUMN_DESCRIPTION));
                product.setPrice(rs.getDouble(COLUMN_PRICE));
                product.setCategory(categoryDao.find(rs.getInt(COLUMN_CATEGORY)));
                product.setImage(rs.getString(COLUMN_IMAGE));
                return product;
            }

        });
        return products;
    }

    @Override
    public int searchCount(ProductSearchOptions options) {
        StringBuilder jpql = new StringBuilder("SELECT COUNT(*) FROM " + TABLE_NAME + " e WHERE 1=1 ");

        appendPredicate(jpql, options);
        List<String> query = new ArrayList<String>();
        setParameters(query, options);

        if (query.size() == 0) {
            return manager.queryForObject(jpql.toString(), Integer.class);
        } else {
            return manager.queryForObject(jpql.toString(), Integer.class, query.toArray());
        }

    }

    private void appendPredicate(StringBuilder jpql, ProductSearchOptions options) {

        if (options.getDescription() != null && options.getDescription().length() > 0) {
            jpql.append(" and lower(e." + COLUMN_DESCRIPTION + ") like ?");
        }
        if (options.getName() != null && options.getName().length() > 0) {
            jpql.append(" and lower(e." + COLUMN_NAME + ") like ?");
        }
        if (options.getPriceMin() != null && options.getPriceMin() > 0) {
            jpql.append(" and e." + COLUMN_PRICE + " >= ?");
        }
        if (options.getPriceMax() != null && options.getPriceMax() > 0) {
            jpql.append(" and e." + COLUMN_PRICE + " <= ?");
        }
        if (options.getCategory() != null && options.getCategory() != 0) {
            jpql.append(" and e." + COLUMN_CATEGORY + " = ?");
        }
        if (options.getCategoriesId() != null && !options.getCategoriesId().isEmpty()) {
            for (int id : options.getCategoriesId()) {
                if (id == options.getCategoriesId().get(0))
                    jpql.append(" and ");
                else
                    jpql.append(" or ");
                jpql.append("e." + COLUMN_CATEGORY + " = ?");
            }
        }
    }

    private void setParameters(List<String> query, ProductSearchOptions options) {
        if (options.getDescription() != null && options.getDescription().length() > 0) {
            query.add("%" + options.getDescription().toLowerCase() + "%");
        }
        if (options.getName() != null && options.getName().length() > 0) {
            query.add("%" + options.getName().toLowerCase() + "%");
        }
        if (options.getPriceMin() != null && options.getPriceMin() > 0) {
            query.add(options.getPriceMin().toString());
        }
        if (options.getPriceMax() != null && options.getPriceMax() > 0) {
            query.add(options.getPriceMax().toString());
        }
        if (options.getCategory() != null && options.getCategory() != 0) {
            query.add(options.getCategory().toString());
        }
        if (options.getCategoriesId() != null && !options.getCategoriesId().isEmpty()) {
            for (Integer id : options.getCategoriesId()) {
                query.add(id.toString());
            }
        }
    }

    @Override
    public Product search(String name) {
        Product product =
                manager.queryForObject("SELECT * FROM " + TABLE_NAME + " e WHERE " + COLUMN_NAME + " = ?",
                        new Object[] {name}, new RowMapper<Product>() {
                            @Override
                            public Product mapRow(ResultSet rs, int rowNum) throws SQLException {
                                Product product = new Product();
                                product.setId(rs.getInt(KEY_COLUMN));
                                product.setName(rs.getString(COLUMN_NAME));
                                product.setDescription(rs.getString(COLUMN_DESCRIPTION));
                                product.setPrice(rs.getDouble(COLUMN_PRICE));
                                product.setCategory(categoryDao.find(rs.getInt(COLUMN_CATEGORY)));
                                product.setImage(rs.getString(COLUMN_IMAGE));
                                product.setId(rs.getInt(KEY_COLUMN));
                                return product;
                            }

                        });
        return product;
    }

    @Override
    public Product find(Object entityId) {
        Product product =
                manager.queryForObject("SELECT * FROM " + TABLE_NAME + " WHERE " + KEY_COLUMN + " = ?",
                        new Object[] {entityId}, new RowMapper<Product>() {
                            @Override
                            public Product mapRow(ResultSet rs, int rowNum) throws SQLException {
                                Product product = new Product();
                                product.setId(rs.getInt(KEY_COLUMN));
                                product.setName(rs.getString(COLUMN_NAME));
                                product.setDescription(rs.getString(COLUMN_DESCRIPTION));
                                product.setPrice(rs.getDouble(COLUMN_PRICE));
                                product.setCategory(categoryDao.find(rs.getInt(COLUMN_CATEGORY)));
                                product.setImage(rs.getString(COLUMN_IMAGE));
                                product.setId(rs.getInt(KEY_COLUMN));
                                return product;
                            }
                        });
        return product;
    }

    public List<Product> searchForRequest(Request request) {
        StringBuilder jpql = new StringBuilder("SELECT P.* FROM ");
        jpql.append(TABLE_NAME);
        jpql.append(" P INNER JOIN RQP_REQUEST_PRODUCT RP ON RP.PRODUCT_ID = P.PRO_ID WHERE RP.REQUEST_ID = ?");

        List<Product> products =
                manager.query(jpql.toString(), new Object[] {request.getId()}, new RowMapper<Product>() {

                    @Override
                    public Product mapRow(ResultSet rs, int rowNum) throws SQLException {
                        Product product = new Product();
                        product.setId(rs.getInt(KEY_COLUMN));
                        product.setName(rs.getString(COLUMN_NAME));
                        product.setDescription(rs.getString(COLUMN_DESCRIPTION));
                        product.setPrice(rs.getDouble(COLUMN_PRICE));
                        product.setCategory(categoryDao.find(rs.getInt(COLUMN_CATEGORY)));
                        product.setImage(rs.getString(COLUMN_IMAGE));
                        return product;
                    }

                });
        return products;
    }

}
