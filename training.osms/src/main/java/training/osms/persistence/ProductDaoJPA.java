package training.osms.persistence;

import java.util.List;

import javax.persistence.TypedQuery;

import org.apache.commons.lang3.NotImplementedException;

import training.framework.persistence.AbstractEntityDao;
import training.framework.persistence.IProductDao;
import training.osms.business.Product;
import training.osms.business.ProductSearchOptions;
import training.osms.business.Request;

public class ProductDaoJPA extends AbstractEntityDao<Product, ProductSearchOptions> implements IProductDao {

    public ProductDaoJPA() {
        super(Product.class);
    }

    @Override
    public void appendPredicate(StringBuilder jpql, ProductSearchOptions options) {

        if (options.getDescription() != null && options.getDescription().length() > 0) {
            jpql.append(" and lower(e.description) like :description");
        }
        if (options.getName() != null && options.getName().length() > 0) {
            jpql.append(" and lower(e.name) like :name");
        }
        if (options.getPriceMin() != null && options.getPriceMin() > 0) {
            jpql.append(" and e.price >= :priceMin");
        }
        if (options.getPriceMax() != null && options.getPriceMax() > 0) {
            jpql.append(" and e.price <= :priceMax");
        }
        if (options.getCategory() != null && options.getCategory() != 0) {
            jpql.append(" and e.category.id = :category");
        }
        if (options.getCategoriesId() != null && !options.getCategoriesId().isEmpty()) {
            for (int id : options.getCategoriesId()) {
                if (id == options.getCategoriesId().get(0))
                    jpql.append(" and ");
                else
                    jpql.append(" or ");
                jpql.append("e.category.id = :id");
                jpql.append(id);
            }
        }
    }

    @Override
    public void setParameters(TypedQuery<?> query, ProductSearchOptions options) {
        if (options.getDescription() != null && options.getDescription().length() > 0) {
            query.setParameter("description", "%" + options.getDescription().toLowerCase() + "%");
        }
        if (options.getName() != null && options.getName().length() > 0) {
            query.setParameter("name", "%" + options.getName().toLowerCase() + "%");
        }
        if (options.getPriceMin() != null && options.getPriceMin() > 0) {
            query.setParameter("priceMin", options.getPriceMin());
        }
        if (options.getPriceMax() != null && options.getPriceMax() > 0) {
            query.setParameter("priceMax", options.getPriceMax());
        }
        if (options.getCategory() != null && options.getCategory() != 0) {
            query.setParameter("category", options.getCategory());
        }
        if (options.getCategoriesId() != null && !options.getCategoriesId().isEmpty()) {
            for (int id : options.getCategoriesId()) {
                query.setParameter("id" + id, id);
            }
        }
    }

    @Override
    @Deprecated
    public List<Product> searchForRequest(Request request) {
        throw new NotImplementedException("");
    }
}
