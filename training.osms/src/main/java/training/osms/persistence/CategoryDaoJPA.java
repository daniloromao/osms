package training.osms.persistence;

import java.util.List;

import javax.persistence.TypedQuery;

import training.framework.persistence.AbstractEntityDao;
import training.framework.persistence.ICategoryDao;
import training.osms.business.Category;
import training.osms.business.CategorySearchOptions;

public class CategoryDaoJPA extends AbstractEntityDao<Category, CategorySearchOptions> implements ICategoryDao {

    public CategoryDaoJPA() {
        super(Category.class);
    }

    @Override
    protected void appendPredicate(StringBuilder jpql, CategorySearchOptions options) {
        if (options.getName() != null && options.getName().length() > 0) {
            jpql.append(" and lower(e.name) like :name");
        }
    }

    @Override
    protected void setParameters(TypedQuery<?> query, CategorySearchOptions options) {
        if (options.getName() != null && options.getName().length() > 0) {
            query.setParameter("name", '%' + options.getName().toLowerCase() + '%');
        }
    }

    @Override
    public List<Category> searchCategoryFathers() {
        StringBuilder jpql = new StringBuilder("select e from Category e where e.father.id is null");
        TypedQuery<Category> query = super.manager.createQuery(jpql.toString(), Category.class);
        return query.getResultList();
    }

}
