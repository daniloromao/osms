package training.osms.persistence;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import training.framework.persistence.ICategoryDao;
import training.framework.persistence.IProductDao;
import training.framework.persistence.IRequestDao;
import training.osms.business.Product;
import training.osms.business.Request;
import training.osms.business.RequestSearchOptions;

public class RequestDaoJDBC implements IRequestDao {

    private String TABLE_NAME = "REQ_REQUEST";
    private String TABLE_RELATED = "RQP_REQUEST_PRODUCT";
    private String KEY_PRODUCT_RELATED = "PRODUCT_ID";
    private String KEY_REQUEST_RELATED = "REQUEST_ID";
    private String KEY_COLUMN = "REQ_ID";
    private String COLUMN_TOTAL = "REQ_TO";
    private String COLUMN_DATE = "REQ_DT";

    private JdbcTemplate manager;
    private SimpleJdbcInsert insertRequest;
    private SimpleJdbcInsert insertRequestProduct;

    @Autowired
    private IProductDao productDao;
    @Autowired
    private ICategoryDao categoryDao;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.manager = new JdbcTemplate(dataSource);
        this.insertRequest =
                new SimpleJdbcInsert(dataSource).withTableName(TABLE_NAME).usingColumns(COLUMN_TOTAL, COLUMN_DATE)
                        .usingGeneratedKeyColumns(KEY_COLUMN);
        this.insertRequestProduct =
                new SimpleJdbcInsert(dataSource).withTableName(TABLE_RELATED).usingColumns(KEY_PRODUCT_RELATED,
                        KEY_REQUEST_RELATED);
    }

    @Override
    public void insert(Request request) {
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put(COLUMN_TOTAL, request.getTotal());
        parameters.put(COLUMN_DATE, request.getDate());
        Number newId = insertRequest.executeAndReturnKey(parameters);
        request.setId(newId.intValue());

        parameters.clear();

        for (Product p : request.getProducts()) {
            parameters.put(KEY_PRODUCT_RELATED, p.getId());
            parameters.put(KEY_REQUEST_RELATED, request.getId());

            insertRequestProduct.execute(parameters);
        }

    }

    @Override
    public List<Request> select(RequestSearchOptions options) {

        StringBuilder jpql = new StringBuilder("SELECT * FROM " + TABLE_NAME + " R WHERE 1=1");

        if (options.getInitDate() != null) {
            jpql.append(" AND R." + COLUMN_DATE + " >= ?");
        }
        if (options.getEndDate() != null) {
            jpql.append(" AND R." + COLUMN_DATE + " <= ?");
        }

        jpql.append(" ORDER BY R." + COLUMN_DATE + " DESC");

        List<String> query = new ArrayList<String>();

        if (options.getInitDate() != null) {
            query.add(options.getInitDate().toString());
        }
        if (options.getEndDate() != null) {
            query.add(options.getEndDate().toString());
        }
        if (options.getMaxResults() != null) {
            jpql.append(" LIMIT ? ");
            query.add(options.getMaxResults().toString());
        }
        if (options.getFirstResult() != null) {
            jpql.append(" OFFSET ? ");
            query.add(options.getFirstResult().toString());
        }

        List<Request> requests = manager.query(jpql.toString(), query.toArray(), new RowMapper<Request>() {

            @Override
            public Request mapRow(ResultSet rs, int rowNum) throws SQLException {
                Request request = new Request();
                request.setId(rs.getInt(KEY_COLUMN));
                request.setDate(rs.getDate(COLUMN_DATE));
                request.setTotal(rs.getDouble(COLUMN_TOTAL));

                addProducts(request);
                return request;
            }

        });
        return requests;
    }

    @Override
    public Integer selectCount(RequestSearchOptions options) {

        StringBuilder jpql = new StringBuilder("SELECT COUNT(*) FROM " + TABLE_NAME + " R WHERE 1=1");

        if (options.getInitDate() != null) {
            jpql.append(" AND R.DATE >= ?");
        }
        if (options.getEndDate() != null) {
            jpql.append(" AND R.DATE <= ?");
        }

        List<String> query = new ArrayList<String>();

        if (options.getInitDate() != null) {
            query.add(options.getInitDate().toString());
        }
        if (options.getEndDate() != null) {
            query.add(options.getEndDate().toString());
        }

        return manager.queryForObject(jpql.toString(), Integer.class, query.toArray());

    }

    @Override
    public Request find(Integer id) {
        Request request =
                manager.queryForObject("SELECT * FROM " + TABLE_NAME + " WHERE " + KEY_COLUMN + " = ?",
                        new Object[] {id}, new RowMapper<Request>() {
                            @Override
                            public Request mapRow(ResultSet rs, int rowNum) throws SQLException {
                                Request request = new Request();
                                request.setId(rs.getInt(KEY_COLUMN));
                                request.setTotal(rs.getDouble(COLUMN_TOTAL));
                                request.setDate(rs.getDate(COLUMN_DATE));
                                return request;
                            }

                        });
        addProducts(request);
        return request;
    }

    private void addProducts(Request request) {
        request.setProducts(productDao.searchForRequest(request));
    }

}
