package training.osms.persistence;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import training.framework.persistence.IRequestDao;
import training.osms.business.Request;
import training.osms.business.RequestSearchOptions;

public class RequestDaoJPA implements IRequestDao {

    @PersistenceContext
    private EntityManager manager;

    @Override
    public void insert(Request insert) {
        manager.persist(insert);
    }

    @Override
    public List<Request> select(RequestSearchOptions options) {

        StringBuilder jpql = new StringBuilder("select r from Request r where 1=1");
        if (options.getInitDate() != null) {
            jpql.append(" and r.date >= :initDate");
        }
        if (options.getEndDate() != null) {
            jpql.append(" and r.date <= :endDate");
        }

        jpql.append(" order by r.date desc");

        TypedQuery<Request> query = manager.createQuery(jpql.toString(), Request.class);

        if (options.getInitDate() != null) {
            query.setParameter("initDate", options.getInitDate());
        }
        if (options.getEndDate() != null) {
            query.setParameter("endDate", options.getEndDate());
        }
        if (options.getMaxResults() != null) {
            query.setMaxResults(options.getMaxResults());
        }
        if (options.getFirstResult() != null) {
            query.setFirstResult(options.getFirstResult());
        }

        return query.getResultList();

    }

    @Override
    public Integer selectCount(RequestSearchOptions options) {

        StringBuilder jpql = new StringBuilder("select count(r) from Request r where 1=1");

        if (options.getInitDate() != null) {
            jpql.append(" and r.date >= :initDate");
        }
        if (options.getEndDate() != null) {
            jpql.append(" and r.date <= :endDate");
        }

        TypedQuery<Long> query = manager.createQuery(jpql.toString(), Long.class);

        if (options.getInitDate() != null) {
            query.setParameter("initDate", options.getInitDate());
        }
        if (options.getEndDate() != null) {
            query.setParameter("endDate", options.getEndDate());
        }

        return query.getSingleResult().intValue();

    }

    @Override
    public Request find(Integer id) {
        return manager.find(Request.class, id);
    }

}
