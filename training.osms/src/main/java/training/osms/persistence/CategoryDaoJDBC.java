package training.osms.persistence;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import training.framework.persistence.ICategoryDao;
import training.osms.business.Category;
import training.osms.business.CategorySearchOptions;

public class CategoryDaoJDBC implements ICategoryDao {

    private static String TABLE_NAME = "CAT_CATEGORY";
    private static String KEY_COLUMN = "CAT_ID";
    private static String COLUMN_NAME = "CAT_NM";
    private static String COLUMN_ID_FATHER = "ID_FATHER";
    private JdbcTemplate manager;
    private SimpleJdbcInsert insertCategory;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.manager = new JdbcTemplate(dataSource);
        this.insertCategory =
                new SimpleJdbcInsert(dataSource).withTableName(TABLE_NAME).usingColumns(COLUMN_NAME, COLUMN_ID_FATHER)
                        .usingGeneratedKeyColumns(KEY_COLUMN);
    }

    @Override
    public void insert(Category category) {
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put(COLUMN_NAME, category.getName());
        parameters.put(COLUMN_ID_FATHER, category.getFather() == null ? null : category.getFather().getId());
        Number newId = insertCategory.executeAndReturnKey(parameters);
        category.setId(newId.intValue());
    }

    @Override
    public void update(Category category) {
        manager.update("UPDATE " + TABLE_NAME + " SET " + COLUMN_NAME + " = ?, " + COLUMN_ID_FATHER + " = ?"
                + " WHERE " + KEY_COLUMN + " = ?", category.getName(), category.getFather() == null ? null : category
                .getFather().getId(), category.getId());
    }

    @Override
    public void delete(Category category) {
        String sqlQuery = "DELETE FROM " + TABLE_NAME + " WHERE " + KEY_COLUMN + " = ?";
        manager.update(sqlQuery, category.getId());
    }

    @Override
    public List<Category> search(CategorySearchOptions options) {
        StringBuilder jpql = new StringBuilder("SELECT * FROM ");
        jpql.append(TABLE_NAME);
        jpql.append(" WHERE 1=1 ");

        List<String> param = new ArrayList<String>();

        if (options.getName() != null) {
            jpql.append(" AND ");
            jpql.append(COLUMN_NAME);
            jpql.append(" LIKE ?");
            param.add("%" + options.getName() + "%");
        }

        if (options.getMaxResults() != null) {
            jpql.append(" LIMIT ? ");
            param.add(options.getMaxResults().toString());
        }

        if (options.getFirstResult() != null) {
            jpql.append(" OFFSET ? ");
            param.add(options.getFirstResult().toString());
        }

        List<Category> categories = manager.query(jpql.toString(), param.toArray(), new RowMapper<Category>() {

            @Override
            public Category mapRow(ResultSet rs, int rowNum) throws SQLException {
                Category category = new Category();
                category.setId(rs.getInt(KEY_COLUMN));
                category.setName(rs.getString(COLUMN_NAME));
                setSons(category);
                setFather(category);
                return category;
            }

        });
        return categories;
    }

    @Override
    public int searchCount(CategorySearchOptions options) {
        StringBuilder jpql = new StringBuilder("SELECT COUNT(*) FROM " + TABLE_NAME + " e WHERE 1=1 ");

        appendPredicate(jpql, options);
        List<String> query = new ArrayList<String>();
        setParameters(query, options);

        return manager.queryForObject(jpql.toString(), Integer.class, query.toArray());
    }

    protected void appendPredicate(StringBuilder jpql, CategorySearchOptions options) {
        if (options.getName() != null && options.getName().length() > 0) {
            jpql.append(" and lower(e." + TABLE_NAME + ") like ?");
        }
    }

    protected void setParameters(List<String> query, CategorySearchOptions options) {
        if (options.getName() != null && options.getName().length() > 0) {
            query.add('%' + options.getName().toLowerCase() + '%');
        }
    }

    @Override
    public Category search(String name) {
        Category category =
                manager.queryForObject("SELECT * FROM " + TABLE_NAME + " WHERE " + COLUMN_NAME + " = ?",
                        new Object[] {name}, new RowMapper<Category>() {
                            @Override
                            public Category mapRow(ResultSet rs, int rowNum) throws SQLException {
                                Category category = new Category();
                                category.setId(rs.getInt(KEY_COLUMN));
                                category.setName(rs.getString(COLUMN_NAME));
                                return category;
                            }

                        });
        setSons(category);
        setFather(category);
        return category;
    }

    @Override
    public Category find(Object entityId) {
        try {
            Category category =
                    manager.queryForObject("SELECT * FROM " + TABLE_NAME + " WHERE " + KEY_COLUMN + " = ?",
                            new Object[] {entityId}, new RowMapper<Category>() {
                                @Override
                                public Category mapRow(ResultSet rs, int rowNum) throws SQLException {
                                    Category category = new Category();
                                    category.setId(rs.getInt(KEY_COLUMN));
                                    category.setName(rs.getString(COLUMN_NAME));
                                    category.setFather(new Category(rs.getInt(COLUMN_ID_FATHER)));
                                    return category;
                                }

                            });
            setSons(category);
            setFather(category);
            return category;
        } catch (Exception e) {

        }

        return null;
    }

    private void setFather(Category category) {
        try {
            Category databaseCategory =
                    manager.queryForObject("SELECT * FROM " + TABLE_NAME + " WHERE " + KEY_COLUMN + " = ?",
                            new Object[] {category.getFather() == null ? null : category.getFather().getId()},
                            new RowMapper<Category>() {
                                @Override
                                public Category mapRow(ResultSet rs, int rowNum) throws SQLException {
                                    Category category = new Category();
                                    category.setId(rs.getInt(KEY_COLUMN));
                                    category.setName(rs.getString(COLUMN_NAME));
                                    return category;
                                }

                            });
            if (databaseCategory.getId() != null) {
                category.setFather(databaseCategory);
            }
        } catch (Exception e) {

        }
    }

    private void setSons(Category category) {
        StringBuilder jpql = new StringBuilder("SELECT * FROM ");
        jpql.append(TABLE_NAME);
        jpql.append(" WHERE " + COLUMN_ID_FATHER + " = ? ");

        if (category.getId() == null) {
            return;
        }
        List<Category> categories =
                manager.query(jpql.toString(), new Object[] {category.getId()}, new RowMapper<Category>() {

                    @Override
                    public Category mapRow(ResultSet rs, int rowNum) throws SQLException {
                        Category category = new Category();
                        category.setId(rs.getInt(KEY_COLUMN));
                        category.setName(rs.getString(COLUMN_NAME));
                        setFather(category);
                        return category;
                    }

                });

        if (categories.size() > 0) {
            category.setSons(categories);
        }
    }

    @Override
    public List<Category> searchCategoryFathers() {
        StringBuilder jpql = new StringBuilder("SELECT * FROM ");
        jpql.append(TABLE_NAME);
        jpql.append(" WHERE ");
        jpql.append(COLUMN_ID_FATHER);
        jpql.append(" IS NULL");

        List<Category> categories = manager.query(jpql.toString(), new RowMapper<Category>() {

            @Override
            public Category mapRow(ResultSet rs, int rowNum) throws SQLException {
                Category category = new Category();
                category.setId(rs.getInt(KEY_COLUMN));
                category.setName(rs.getString(COLUMN_NAME));
                return category;
            }

        });
        return categories;
    }

}
