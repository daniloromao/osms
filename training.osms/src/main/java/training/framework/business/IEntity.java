package training.framework.business;

public interface IEntity {

	Integer getId();
	
	String getName();
}
