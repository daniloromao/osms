package training.framework.business;

import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import training.framework.persistence.IEntityDao;

@Component
public abstract class AbstractEntityController <Entity extends IEntity, EntitySearchOptions extends AbstractEntitySearchOptions>{


	protected abstract IEntityDao<Entity, EntitySearchOptions> getDao();
	
	@Transactional
	public void insert(Entity entity){
		insertVerification(entity);
		getDao().insert(entity);
	}
	
	protected abstract void insertVerification(Entity entity);
	
	@Transactional
	public void update(Entity entity) {
		upadateVerification(entity);
		getDao().update(entity);
	}
	
	protected abstract void upadateVerification(Entity entity);

	
	public List<Entity> search(EntitySearchOptions options){
		return getDao().search(options);
	}

	@Transactional
	public void delete(Entity entity) {
		getDao().delete(entity);
	}

	public int searchCount(EntitySearchOptions options) {
		return getDao().searchCount(options);
	}

	public Entity search(String name) {
		return getDao().search(name);
	}

	public Entity search(Integer entityId) {
		return getDao().find(entityId);
	}
}