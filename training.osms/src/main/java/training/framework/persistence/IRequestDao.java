package training.framework.persistence;

import java.util.List;

import training.osms.business.Request;
import training.osms.business.RequestSearchOptions;


public interface IRequestDao {
	
	void insert(Request request);
	
	List<Request> select(RequestSearchOptions options);
	
	Integer selectCount(RequestSearchOptions options);
	
	Request find(Integer id);

}
