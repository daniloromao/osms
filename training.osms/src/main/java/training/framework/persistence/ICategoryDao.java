package training.framework.persistence;

import java.util.List;

import training.osms.business.Category;
import training.osms.business.CategorySearchOptions;

public interface ICategoryDao extends IEntityDao<Category, CategorySearchOptions>{

	List<Category> searchCategoryFathers();
	
}
