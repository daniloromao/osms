package training.framework.persistence;

import java.util.List;

public interface IEntityDao <Entity, EntitySearchOptions>{

	void insert(Entity entity);
	
	void update(Entity entity);
	
	void delete(Entity entity);
	
	List<Entity> search(EntitySearchOptions options);
	
	int searchCount(EntitySearchOptions options);
	
	Entity find(Object entityId);
	
	Entity search(String name);
}
