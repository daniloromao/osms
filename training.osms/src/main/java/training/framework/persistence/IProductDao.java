package training.framework.persistence;

import java.util.List;

import training.osms.business.Product;
import training.osms.business.ProductSearchOptions;
import training.osms.business.Request;

public interface IProductDao extends IEntityDao<Product, ProductSearchOptions> {

    List<Product> searchForRequest(Request request);
}
