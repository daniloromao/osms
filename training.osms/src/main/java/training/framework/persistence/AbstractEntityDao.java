package training.framework.persistence;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import javax.persistence.TypedQuery;

import training.framework.business.AbstractEntitySearchOptions;

public abstract class AbstractEntityDao<Entity, EntitySearchOptions extends AbstractEntitySearchOptions> {

    @PersistenceContext
    protected EntityManager manager;

    @PersistenceUnit
    private EntityManagerFactory factory;
    private Class<Entity> entityClass;

    public AbstractEntityDao(Class<Entity> entityClass) {
        this.entityClass = entityClass;
    }

    public void insert(Entity entity) {
        manager.persist(entity);
    }

    public void update(Entity entity) {
        manager.merge(entity);
    }

    public void delete(Entity entity) {
        Object entityId = factory.getPersistenceUnitUtil().getIdentifier(entity);
        Entity databaseEntity = manager.find(entityClass, entityId);
        manager.remove(databaseEntity);
    }

    public List<Entity> search(EntitySearchOptions options) {

        StringBuilder jpql = new StringBuilder("select e from ");
        jpql.append(entityClass.getName());
        jpql.append(" e where 1=1");

        appendPredicate(jpql, options);

        TypedQuery<Entity> query = manager.createQuery(jpql.toString(), entityClass);

        setParameters(query, options);

        if (options.getFirstResult() != null) {
            query.setFirstResult(options.getFirstResult());
        }
        if (options.getMaxResults() != null) {
            query.setMaxResults(options.getMaxResults());
        }

        return query.getResultList();
    }

    public int searchCount(EntitySearchOptions options) {

        StringBuilder jpql = new StringBuilder("select count(*) from ");
        jpql.append(entityClass.getName());
        jpql.append(" e where 1=1");

        appendPredicate(jpql, options);
        System.out.println(jpql.toString());
        TypedQuery<Long> query = manager.createQuery(jpql.toString(), Long.class);

        setParameters(query, options);

        return query.getSingleResult().intValue();
    }

    public Entity search(String name) {

        StringBuilder jpql = new StringBuilder("select e from ");
        jpql.append(entityClass.getName());
        jpql.append(" e where lower(e.name) = :name");

        TypedQuery<Entity> query = manager.createQuery(jpql.toString(), entityClass);

        query.setParameter("name", name.toLowerCase());

        List<Entity> entities = query.getResultList();

        if (entities.isEmpty()) {
            return null;
        } else {
            return entities.get(0);
        }
    }

    public Entity find(Object entityId) {
        return manager.find(entityClass, entityId);
    }

    protected abstract void appendPredicate(StringBuilder jpql, EntitySearchOptions options);

    protected abstract void setParameters(TypedQuery<?> query, EntitySearchOptions options);
}